package com.saggie.commons.ws.utils;

public class SaggieObjectUtils {
    public static boolean isNull(Object object){
        return object == null;
    }

    public static boolean isNotNull(Object object){
        return object != null;
    }
}
