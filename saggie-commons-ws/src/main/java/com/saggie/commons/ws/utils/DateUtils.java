/**
 * Copyright (c) 2015, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by bilal on 2/9/16.
 */
package com.saggie.commons.ws.utils;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtils {
    private static Logger logger = LogManager.getLogger(DateUtils.class);

    // Init ---------------------------------------------------------------------------------------

    private static final Map<String, String> DATE_FORMAT_REGEXPS = new HashMap<String, String>() {{
        put("^\\d{8}$", "yyyyMMdd");
        put("^\\d{1,2}-\\d{1,2}-\\d{4}$", "dd-MM-yyyy");
        put("^\\d{4}-\\d{1,2}-\\d{1,2}$", "yyyy-MM-dd");
        put("^\\d{1,2}/\\d{1,2}/\\d{4}$", "MM/dd/yyyy");
        put("^\\d{4}/\\d{1,2}/\\d{1,2}$", "yyyy/MM/dd");
        put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}$", "dd MMM yyyy");
        put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}$", "dd MMMM yyyy");
        put("^\\d{12}$", "yyyyMMddHHmm");
        put("^\\d{8}\\s\\d{4}$", "yyyyMMdd HHmm");
        put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}$", "dd-MM-yyyy HH:mm");
        put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy-MM-dd HH:mm");
        put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}$", "MM/dd/yyyy HH:mm");
        put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy/MM/dd HH:mm");
        put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMM yyyy HH:mm");
        put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMMM yyyy HH:mm");
        put("^\\d{14}$", "yyyyMMddHHmmss");
        put("^\\d{8}\\s\\d{6}$", "yyyyMMdd HHmmss");
        put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd-MM-yyyy HH:mm:ss");
        put("^\\d{4}-\\d{1,2}-\\d{1,2}\\S\\d{1,2}:\\d{2}:\\d{2}.\\d{3}\\S$", "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        put("^\\d{4}-\\d{1,2}-\\d{1,2}\\S\\d{1,2}:\\d{2}$", "yyyy-MM-dd'T'HH:mm'Z'");
        put("^\\d{4}-\\d{1,2}-\\d{1,2}\\S\\d{1,2}:\\d{2}\\S$", "yyyy-MM-dd'T'HH:mm'Z'");
        put("^\\d{4}-\\d{1,2}-\\d{1,2}\\S\\d{1,2}:\\d{2}:\\d{2}.\\d{2}\\S$", "yyyy-MM-dd'T'HH:mm:ss.SS'Z'");
        put("^\\d{4}-\\d{1,2}-\\d{1,2}\\S\\d{1,2}:\\d{2}:\\d{2}.\\d{1}\\S$", "yyyy-MM-dd'T'HH:mm:ss.S'Z'");
        put("^\\d{4}-\\d{1,2}-\\d{1,2}\\S\\d{1,2}:\\d{2}:\\d{2}\\S$", "yyyy-MM-dd'T'HH:mm:ss'Z'");
        put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy-MM-dd HH:mm:ss");
        put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "MM/dd/yyyy HH:mm:ss");
        put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy/MM/dd HH:mm:ss");
        put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}.\\d{3}$", "yyyy/MM/dd HH:mm:ss.SSS");
        put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMM yyyy HH:mm:ss");
        put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMMM yyyy HH:mm:ss");
        put("^([\\w:/]+\\s[+\\-]\\d{4})$", "dd/MMM/yyyy:HH:mm:ss ZZZZ");
        put("[A-Za-z]+ [A-Za-z]+ \\d?\\d \\d{1,2}:\\d{1,2}:\\d{1,2} [A-Za-z]{3} \\d{4}", "EEE MMM dd HH:mm:ss Z yyyy");
    }};

    private DateUtils() {
        // Utility class, hide the constructor.
    }

    // Converters ---------------------------------------------------------------------------------

    /**
     * Convert the given date to a Calendar object. The TimeZone will be operating
     * system's timezone.
     * @param date The date to be converted to Calendar.
     * @return The Calendar object set to the given date and using the local timezone.
     */
    public static Calendar toCalendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.setTime(date);
        return calendar;
    }

    /**
     * Convert the given date to a Calendar object with the given timezone.
     * @param date The date to be converted to Calendar.
     * @param timeZone The timezone to be set in the Calendar.
     * @return The Calendar object set to the given date and timezone.
     */
    public static Calendar toCalendar(Date date, TimeZone timeZone) {
        Calendar calendar = toCalendar(date);
        calendar.setTimeZone(timeZone);
        return calendar;
    }

    /**
     * Parse the given date and returns a date instance based on the given
     * input. This makes use of the {@link DateUtils#findDateFormat(String)} to determine
     * the pattern to be used for parsing.
     * @param date The date as string to be parsed to date object.
     * @return The parsed date object.
     * @throws ParseException If the date format pattern of the given date string is unknown, or if
     * the given date string or its actual date is invalid based on the date format pattern.
     */
    public static Date parse(String date) throws ParseException {
        String dateFormat = findDateFormat(date);
        if (dateFormat == null) {
            throw new ParseException("Unknown date format.", 0);
        }
        return parse(date, dateFormat);
    }

    /**
     * Validate the actual date of the given date string based on the given date format pattern and
     * return a date instance based on the given date string.
     * @param date The date string.
     * @param format The date format pattern which should respect the SimpleDateFormat rules.
     * @return The parsed date object.
     * @throws ParseException If the given date string or its actual date is invalid based on the
     * given date format pattern.
     * @see SimpleDateFormat
     */
    public static Date parse(String date, String format) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        simpleDateFormat.setLenient(false); // Don't automatically convert invalid date.
        return simpleDateFormat.parse(date);
    }

    public static Date parse(String date, String format, TimeZone timeZone) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        simpleDateFormat.setTimeZone(timeZone);
        simpleDateFormat.setLenient(false); // Don't automatically convert invalid date.
        return simpleDateFormat.parse(date);
    }


    public static Date parse(String date, TimeZone timeZone) throws ParseException {
        String dateFormat = findDateFormat(date);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        simpleDateFormat.setTimeZone(timeZone);
        simpleDateFormat.setLenient(false); // Don't automatically convert invalid date.
        return simpleDateFormat.parse(date);
    }

    public static String dateToString(Date date, String format){
        return DateFormatUtils.format(date, format);
    }



    // Validators ---------------------------------------------------------------------------------

    /**
     * Checks whether the actual date of the given date string is valid. This makes use of the
     * {@link DateUtils#findDateFormat(String)} to determine the SimpleDateFormat pattern to be
     * used for parsing.
     * @param date The date string.
     * @return True if the actual date of the given date string is valid.
     */
    public static boolean isValidDate(String date) {
        try {
            parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    /**
     * Checks whether the actual date of the given date string is valid based on the given date
     * format pattern.
     * @param date date string.
     * @param format The format pattern which should respect the SimpleDateFormat rules.
     * @return True if the actual date of the given date string is valid based on the given
     * format pattern.
     * @see SimpleDateFormat
     */
    public static boolean isValidDate(String date, String format) {
        try {
            parse(date, format);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    // Checkers -----------------------------------------------------------------------------------

    /**
     * Returns null if format is unknown else return date format string.
     * You can simply extend DateUtils with more
     * formats if needed.
     * @param date The date string to determine the SimpleDateFormat pattern for.
     * @return The matching SimpleDateFormat pattern, or null if format is unknown.
     * @see SimpleDateFormat
     */
    private static String findDateFormat(String date) {
        for (String regexp : DATE_FORMAT_REGEXPS.keySet()) {
            if (date.toLowerCase().matches(regexp)) {
                return DATE_FORMAT_REGEXPS.get(regexp);
            }
        }
        return null; // Unknown format.
    }

    /**
     * compares two date. if date1 is after date2 returns 1
     * if data1 is before date2 returns -1
     * if date1 is equal to date2 return 0
     * @param date1
     * @param date2
     * @return -1, 0, 1
     */
    public static int compareDate(Date date1, Date date2){
        return date1.compareTo(date2) > 0 ? 1 : date1.compareTo(date2) < 0 ? -1 : 0;
    }


    /**
     * method will detect format of date itself and tries
     * to parse the date using {@link DateUtils#findDateFormat(String)}
     * and compare using {@link DateUtils#compareDate(Date, Date)}
     * @param date1
     * @param date2
     * @return -1, 0, 1
     * @throws ParseException If the given date string or its actual date is invalid based on the
     * given date format pattern.
     * @see SimpleDateFormat
     */
    public static int compareDate(String date1, String date2) throws ParseException{
        Date start = parse(date1);
        Date end = parse(date2);
        return compareDate(start, end);
    }
}
