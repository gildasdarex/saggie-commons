/**
 * Copyright (c) 2016, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 *
 * @author tpoola on 1/22/2016
 */
package com.saggie.commons.ws.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class TimeUtils {



    private static Logger logger = LogManager.getLogger(TimeUtils.class);
    private static String dateFormat = "yyyy-MM-dd HH:mm:ss";

    private TimeUtils(){}

    public static long convertToUTC(Date date){
        ZonedDateTime zdt = ZonedDateTime.ofInstant(date.toInstant(), ZoneId.of("UTC"));
        return zdt.toEpochSecond() * 1000;
    }

    public static long convertToRequiredZone(Date date, ZoneId zoneId){
        ZonedDateTime zdt = ZonedDateTime.ofInstant(date.toInstant(), zoneId);
        return zdt.toEpochSecond() * 1000;
    }



    public static String longToHours(long time) throws ParseException {
        /*Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.setTimeInMillis(time * 1000);

        int year = calendar.get(Calendar.YEAR);
        int day = calendar.get(Calendar.DATE);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        int millisecond = calendar.get(Calendar.MILLISECOND);

        return hour+":"+minute+":"+second+":"+millisecond;*/
        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(time),
                TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(time)),
                TimeUnit.MILLISECONDS.toSeconds(time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)));
        return hms;

    }


    public static String longToDate(long time, String format, TimeZone timeZone){
        DateFormat formatter= new SimpleDateFormat(format);
        formatter.setTimeZone(timeZone);
        Date date = new Date(time);
        return formatter.format(date);
    }

    public static String longToDate(long time, String format){
        DateFormat formatter= new SimpleDateFormat(format);
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        return longToDate( time, format, timeZone);
    }

    public static String longToDate(long time){
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        return longToDate( time, dateFormat, timeZone);
    }

    public static Timestamp addDay(Timestamp timestamp , Integer days){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp.getTime());

        // add  days
        cal.setTimeInMillis(timestamp.getTime());
        cal.add(Calendar.DAY_OF_MONTH, days);
        timestamp = new Timestamp(cal.getTime().getTime());

        return timestamp;

    }
}
