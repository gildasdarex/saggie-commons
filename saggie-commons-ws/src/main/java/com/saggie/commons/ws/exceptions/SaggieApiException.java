package com.saggie.commons.ws.exceptions;


import com.saggie.commons.ws.enums.ApiContext;
import com.saggie.commons.ws.enums.ApiResponseErrorCategory;

public class SaggieApiException  extends Exception{
    private ApiContext apiContext;
    private ApiResponseErrorCategory apiResponseErrorCategory;
    private String message;
    //private String status;
    private Object data;

    public SaggieApiException(ApiContext apiContext, ApiResponseErrorCategory apiResponseErrorCategory, String message) {
        this.apiContext = apiContext;
        this.apiResponseErrorCategory = apiResponseErrorCategory;
        this.message = message;
    }

    public SaggieApiException(ApiContext apiContext, ApiResponseErrorCategory apiResponseErrorCategory, String message, Object data) {
        this.apiContext = apiContext;
        this.apiResponseErrorCategory = apiResponseErrorCategory;
        this.message = message;
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ApiContext getApiContext() {
        return apiContext;
    }

    public void setApiContext(ApiContext apiContext) {
        this.apiContext = apiContext;
    }

    public ApiResponseErrorCategory getApiResponseErrorCategory() {
        return apiResponseErrorCategory;
    }

    public void setApiResponseErrorCategory(ApiResponseErrorCategory apiResponseErrorCategory) {
        this.apiResponseErrorCategory = apiResponseErrorCategory;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



}
