package com.saggie.commons.ws.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.List;

public class SaggieCollectionUtils {
    public static boolean isEmpty(Collection entity) {
        return (null==entity || entity.isEmpty());
    }

    public static boolean isEmpty(Object [] objects) {
        return (null==objects || objects.length == 0);
    }

    public static boolean isNotEmpty(List entityList) {
        return (null != entityList && (!entityList.isEmpty()));
    }

    public static boolean isNotEmpty(Collection collection){
            return (null != collection && (!collection.isEmpty()));
    }

    public static boolean isNull(Collection<? extends Object> entity) {return entity == null; }

    public static boolean isNotNull(Collection<? extends Object> entity) {return entity != null; }

    public static boolean isNotEmpty(Object [] objects) {
        return (null!=objects && objects.length != 0);
    }

    public static <S, T extends Iterable<S>> String print(T list){
        StringBuilder stringBuilder = new StringBuilder();
        for (Object element : list){
            stringBuilder.append(element).append(",");
        }

        return stringBuilder.toString();
    }

    public static String printArray(Object[] array) {
        return StringUtils.join(array, ',');
    }
}
