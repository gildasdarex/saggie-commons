package com.saggie.commons.ws.enums;


public enum ApiResponseStatus {
    // STATUS CODES
    SUCCESS,
    PARTIAL_SUCCESS,
    FAILURE
}
