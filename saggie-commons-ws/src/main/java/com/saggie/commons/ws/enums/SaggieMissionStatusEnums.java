package com.saggie.commons.ws.enums;


/**
 * Copyright (c) 2018, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by darextossa on 9/29/18
 */
public enum SaggieMissionStatusEnums implements EnumConstants {

    NEW("NEW"),
    IN_PROGRESS("IN_PROGRESS"),
    AGENT_DEAD("AGENT_DEAD"),
    AGENT_NOT_FOUND("AGENT_NOT_FOUND"),
    FAILED("FAILED"),
    COMPLETED("COMPLETED");


    private String name;

    SaggieMissionStatusEnums(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getUidValue() {
        switch (this) {
            case NEW: return 6;
            case FAILED: return 2;
            case IN_PROGRESS: return 3;
            case AGENT_DEAD: return 5;
            case AGENT_NOT_FOUND: return 4;
            case COMPLETED: return 1;
            default: return INVALID_ENUM_VALUE;
        }
    }

    public static SaggieMissionStatusEnums fromIntValue(int value) {
// WARNING: int values must match the static data values in table t_algo_type.uid for the name
        switch (value) {
            case 6: return SaggieMissionStatusEnums.NEW;
            case 3: return SaggieMissionStatusEnums.IN_PROGRESS;
            case 5: return SaggieMissionStatusEnums.AGENT_DEAD;
            case 4: return SaggieMissionStatusEnums.AGENT_NOT_FOUND;
            case 1: return SaggieMissionStatusEnums.COMPLETED;
            case 2: return SaggieMissionStatusEnums.FAILED;
            default:
                throw new RuntimeException("Invalid SaggieMissionStatusEnums uid value:" + value);
        }
    }
}
