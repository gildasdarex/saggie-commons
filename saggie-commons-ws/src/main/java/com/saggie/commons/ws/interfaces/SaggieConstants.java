package com.saggie.commons.ws.interfaces;

public interface SaggieConstants {

    String API_CONTEXT = "api_context";
    String USER_ID = "user_id";
    String HTTP_STATUS = "http_status";

}
