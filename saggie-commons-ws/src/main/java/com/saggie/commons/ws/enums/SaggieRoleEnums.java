package com.saggie.commons.ws.enums;


/**
 * Copyright (c) 2018, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by darextossa on 9/29/18
 */
public enum SaggieRoleEnums implements EnumConstants {

    NONE("NONE"),
    ADMIN("ADMIN"),
    LEADER("LEADER"),
    AGENT("AGENT");


    private String name;

    SaggieRoleEnums(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getUidValue() {
        switch (this) {
            case NONE: return 0;
            case ADMIN: return 1;
            case LEADER: return 2;
            case AGENT: return 3;
            default: return INVALID_ENUM_VALUE;
        }
    }

    public static SaggieRoleEnums fromIntValue(int value) {
// WARNING: int values must match the static data values in table t_algo_type.uid for the name
        switch (value) {
            case 0: return SaggieRoleEnums.NONE;
            case 1: return SaggieRoleEnums.ADMIN;
            case 2: return SaggieRoleEnums.LEADER;
            case 3: return SaggieRoleEnums.AGENT;
            default:
                throw new RuntimeException("Invalid SaggieRoleEnums uid value:" + value);
        }
    }
}
