/**
 * Copyright (c) 2017, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 *
 * @author tpoola on 4/20/2017
 */
package com.saggie.commons.ws.utils;

import com.google.common.base.Strings;
import com.saggie.commons.ws.enums.ApiContext;
import com.saggie.commons.ws.enums.ApiResponseErrorCategory;
import com.saggie.commons.ws.exceptions.SaggieApiException;

import java.util.List;

public class SaggieDbAssert {
    public static final String prefixMsg = "Reason - Not Found in Db: ";
    public static final String postFixMsgWasNull = " Cause: Object was null";
    public static final String postFixMsgWasEmpty = " Cause: List was empty";


    public static void isNull(ApiContext apiContext, Object entity, String suffixMsg) throws SaggieApiException {
        if(null==entity) {
            String message = Strings.isNullOrEmpty(suffixMsg)?prefixMsg:suffixMsg;
            throw  new SaggieApiException(apiContext, ApiResponseErrorCategory.DATA_NOT_FOUND, message, entity);
        }
    }

    public static void isNullOrEmpty(ApiContext apiContext, List entityList, String suffixMsg) throws SaggieApiException {
        SaggieDbAssert.isNull(apiContext, entityList, suffixMsg);
        if(entityList.isEmpty()) {
            throw  new SaggieApiException(apiContext, ApiResponseErrorCategory.DATA_NOT_FOUND, postFixMsgWasEmpty, entityList);
        }
    }
}
