package com.saggie.commons.ws.utils;

import com.google.gson.*;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JSONUtils {
    public static String toJSON(Object object) {
        Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().disableHtmlEscaping().create();
        JsonElement jsonElement = gson.toJsonTree(object, object.getClass());
        JsonObject jsonObject = new JsonObject();
        jsonObject.add(object.getClass().getName(), jsonElement);
        JsonParser jsonParser = new JsonParser();
        jsonElement = jsonParser.parse(jsonObject.toString());
        return gson.toJson(jsonElement);
    }

    public static String toJSONWithoutClassName(Object object) {
        Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().disableHtmlEscaping().create();
        return gson.toJson(object);
    }

    public static String toJSON(Object object, Type type) {
        Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().disableHtmlEscaping().create();
        return gson.toJson(object, type);
    }


    public static String toJSON(Writer writer, Object object) throws Exception{
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        String json = toJSON(object);
        bufferedWriter.write(json);
        bufferedWriter.close();
        return json;
    }

    public static String toJSON(FileReader fileReader) throws IOException{
        Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().disableHtmlEscaping().create();
        JsonParser parser = new JsonParser();
        JsonElement jsonElement = parser.parse(fileReader);
        return gson.toJson(jsonElement);
    }

    public static byte[] toJSONasBytes(Object object) {
        return JSONUtils.toJSON(object).getBytes(StandardCharsets.UTF_8);
    }

    public static <T> T fromJSON(Class<T> theClass, String json) {
        return JSONUtils.fromJSON(theClass.getName(), theClass, json);
    }

    public static <T> T fromJSON(Class<T> theClass, byte[] jsonAsBytes) {
        return JSONUtils.fromJSON(theClass, new String(jsonAsBytes,StandardCharsets.UTF_8));
    }

    public static <T> T fromJSON(String classAliasName, Class<T> theClass, String json) {
        Gson gson = new GsonBuilder().enableComplexMapKeySerialization().create();
        return gson.fromJson(json, theClass);
    }

    public static <T> T fromJSON(String classAliasName, Class<T> theClass, byte[] jsonAsBytes) {
        return JSONUtils.fromJSON(classAliasName, theClass, new String(jsonAsBytes,StandardCharsets.UTF_8));
    }

    public static <T> T fromJSON(Reader reader, Class<T> theClass) throws IOException {
        return new Gson().fromJson(reader, theClass);
    }


    public static <T> T fromJSON(Reader reader, Type type) throws IOException {
        return new Gson().fromJson(reader, type);
    }

    public static <T> T fromJSON(String json, Type theClass) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(json, theClass);
    }
    public static <T> T fromJSON(byte[] jsonAsBytes, Type theClass) {
        return JSONUtils.fromJSON(new String(jsonAsBytes, StandardCharsets.UTF_8), theClass);
    }

    public static <T> T fromJSON(String json, Class<T> theClass) {
        Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
        JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
        return gson.fromJson(jsonObject.get(theClass.getName()), theClass);
    }

    public static <T> T fromJSON(byte[] jsonAsBytes, Class<T> theClass) {
        return JSONUtils.fromJSON(new String(jsonAsBytes, StandardCharsets.UTF_8), theClass);
    }

    private static String createAndWriteStringToFile(String fileName, String json) throws IOException {
        Path jsonFilePath = Paths.get(fileName);
        Files.createDirectories(jsonFilePath.getParent());
        Files.deleteIfExists(jsonFilePath);
        Files.createFile(jsonFilePath);
        PrintWriter writer = new PrintWriter(new File(fileName), StandardCharsets.UTF_8.toString());
        writer.write(json);
        writer.close();
        return json;
    }

    public static String createAndWriteToFile(String fileName, Object object) throws IOException{
        if(object instanceof String) return createAndWriteStringToFile( fileName, (String)object);
        return createAndWriteToFile(fileName, object, object.getClass());
    }

    public static String createAndWriteToFile(String fileName, Object object, boolean noClassName) throws IOException{
        if(noClassName) return createAndWriteToFile(fileName, object);

        Path jsonFilePath = Paths.get(fileName);
        Files.createDirectories(jsonFilePath.getParent());
        Files.deleteIfExists(jsonFilePath);
        Files.createFile(jsonFilePath);
        PrintWriter writer = new PrintWriter(new File(fileName), StandardCharsets.UTF_8.toString());
        String json = toJSON(object);
        writer.write(json);
        writer.close();
        return json;
    }

    public static String createAndWriteToFile(String fileName, byte[] jsonAsBytes) throws IOException {
        return JSONUtils.createAndWriteToFile(fileName, new String(jsonAsBytes, StandardCharsets.UTF_8));
    }

    public static <T> String createAndWriteToFile(String fileName, T object, Type type) throws IOException{
        Path jsonFilePath = Paths.get(fileName);
        Files.createDirectories(jsonFilePath.getParent());
        Files.deleteIfExists(jsonFilePath);
        Files.createFile(jsonFilePath);
        return writeToFile(fileName, object, type);
    }

    public static <T> String writeToFile(String fileName, T object, Type type) throws IOException{
        Path jsonFilePath = Paths.get(fileName);
        if (!Files.exists(jsonFilePath))
            createAndWriteToFile(fileName, object, type);
        PrintWriter writer = new PrintWriter(new File(fileName), StandardCharsets.UTF_8.toString());
        String json = toJSON(object, type);
        writer.write(json);
        writer.close();
        return json;
    }

}
