package com.saggie.commons.ws.utils;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public class JavaUtils {
    public static List<Field> getAllFields(List<Field> fields, Class<?> type)       {
        fields.addAll(Arrays.asList(type.getDeclaredFields()));

        if (type.getSuperclass() != null) {
            fields = getAllFields(fields, type.getSuperclass());
        }

        return fields;
    }

    public static boolean isEmpty(String value) {
        return value == null || value.isEmpty();
    }

    public static boolean isNotEmpty(String value) {
        return !isEmpty(value);
    }
}
