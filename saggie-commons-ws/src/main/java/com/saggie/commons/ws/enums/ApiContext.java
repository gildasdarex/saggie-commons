package com.saggie.commons.ws.enums;

/**
 * Copyright (c) 2018, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by darextossa on 9/22/18
 */
public enum ApiContext {
    NONE,
    AUTHENTIFICATION,

    CREATE_OR_UPDATE_USER,
    DELETE_USER,
    GET_USER,
    LIST_USERS,

    CREATE_OR_UPDATE_MISSION,
    DELETE_MISSION,
    GET_MISSION,
    LIST_MISSIONS,

    DATA_INTEGRITY,
    VALIDATION

}
