package com.saggie.commons.ws.models;

import com.saggie.commons.ws.enums.ApiContext;
import com.saggie.commons.ws.enums.ApiResponseErrorCategory;
import lombok.*;


@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ApiResponseInfo {
    private Integer apiResponseCode;
    private String apiResponseMessage;
    private ApiContext apiContext;
    private ApiResponseErrorCategory apiResponseErrorCategory;
}
