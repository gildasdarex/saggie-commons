
package com.saggie.commons.ws.models;


import com.saggie.commons.ws.enums.ApiContext;
import com.saggie.commons.ws.interfaces.SaggieConstants;
import com.saggie.commons.ws.utils.JSONUtils;
import com.saggie.commons.ws.utils.SaggieObjectUtils;


import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * usage: new SaggieRequestContext.SaggieRequestContextBuilder().addUserId(integer).build();
 */
public class SaggieRequestContext extends ConcurrentHashMap<String, Object> implements SaggieConstants {


    private SaggieRequestContext(ApiContext apiContext, Integer userId) {
        if(SaggieObjectUtils.isNotNull(apiContext)) put(API_CONTEXT, apiContext);
        if(SaggieObjectUtils.isNotNull(userId)) put(USER_ID, userId);
    }


    private SaggieRequestContext(SaggieRequestContextBuilder saggieRequestContextBuilder){
        super(saggieRequestContextBuilder);
    }

    public Integer getUserId(){ return (Integer) get(USER_ID); }

    public boolean hasUserId(){ return containsKey(USER_ID); }

    public ApiContext getApiContext(){ return (ApiContext) get(API_CONTEXT); }

    public boolean hasApiContext(){ return containsKey(API_CONTEXT); }


    public String toJSON() {
        return JSONUtils.toJSONWithoutClassName(this);
    }

    public static SaggieRequestContext fromJSON(String json) {
        return JSONUtils.fromJSON(json, SaggieRequestContext.class);
    }

    @Override
    public String toString() {
        return toJSON();
    }


    public static class SaggieRequestContextBuilder extends HashMap<String, Object> {

        public SaggieRequestContextBuilder addApiContext(ApiContext apiContext){
            put(API_CONTEXT, apiContext);
            return this;
        }


        public SaggieRequestContextBuilder addUserId(Integer userId){
            put(USER_ID, userId);
            return this;
        }

        public SaggieRequestContext build(){
            return new SaggieRequestContext(this);
        }

    }
}

