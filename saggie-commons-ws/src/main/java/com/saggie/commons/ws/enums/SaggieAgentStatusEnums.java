package com.saggie.commons.ws.enums;


/**
 * Copyright (c) 2018, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by darextossa on 9/29/18
 */
public enum SaggieAgentStatusEnums implements EnumConstants {

    ON_MISSION("ON_MISSION"),
    HOME("HOME"),
    NONE("NONE"),
    ALIVE("ALIVE"),
    DEAD("DEAD"),
    MISSING("MISSING");


    private String name;

    SaggieAgentStatusEnums(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getUidValue() {
        switch (this) {
            case ON_MISSION: return 1;
            case HOME: return 2;
            case NONE: return 3;
            case ALIVE: return 4;
            case DEAD: return 5;
            case MISSING: return 6;
            default: return INVALID_ENUM_VALUE;
        }
    }

    public static SaggieAgentStatusEnums fromIntValue(int value) {
// WARNING: int values must match the static data values in table t_algo_type.uid for the name
        switch (value) {
            case 1: return SaggieAgentStatusEnums.ON_MISSION;
            case 2: return SaggieAgentStatusEnums.HOME;
            case 3: return SaggieAgentStatusEnums.NONE;
            case 4: return SaggieAgentStatusEnums.ALIVE;
            case 5: return SaggieAgentStatusEnums.DEAD;
            case 6: return SaggieAgentStatusEnums.MISSING;
            default:
                throw new RuntimeException("Invalid SaggieAgentStatusEnums uid value:" + value);
        }
    }
}
