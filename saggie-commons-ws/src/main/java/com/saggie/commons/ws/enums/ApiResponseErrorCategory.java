package com.saggie.commons.ws.enums;


public enum ApiResponseErrorCategory {
    NONE,
    AUTHORIZATION,
    BAD_CREDENTIALS,
    TOKEN_EXPIRED,
    TOKEN_INVALID,
    AUTHENTICATION_ERROR,
    DATA_NOT_FOUND,
    USER_DEACTIVATED,
    ACCOUNT_SUSPENDED,
    USER,
    MISSION,
    DATABASE,
    INVALID_DATA,
    MISSING_DATA
}
