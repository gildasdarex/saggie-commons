package com.saggie.commons.ws.enums;

public interface EnumConstants {
    int NOT_FOUND_VALUE = -10000;
    int INVALID_ENUM_VALUE = -99999;
}
