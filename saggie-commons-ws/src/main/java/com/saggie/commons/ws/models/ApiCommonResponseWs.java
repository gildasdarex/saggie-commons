/**
 * Copyright (c) 2018, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by darextossa on 9/22/18
 */
package com.saggie.commons.ws.models;

import com.saggie.commons.ws.enums.ApiResponseStatus;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ApiCommonResponseWs extends ApiResponseInfo {
    private ApiResponseStatus status;
    private Object data;
}
