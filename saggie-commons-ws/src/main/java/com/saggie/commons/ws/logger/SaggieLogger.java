package com.saggie.commons.ws.logger;

import com.saggie.commons.ws.utils.JSONUtils;
import com.saggie.commons.ws.utils.SaggieObjectUtils;
import com.saggie.commons.ws.models.SaggieRequestContext;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;


public class SaggieLogger {

    public static void logRequestIn(Logger rlogger, Level level, SaggieRequestContext saggieRequestContext, HttpServletRequest request){
        String logStr = " Incoming request";
        if(SaggieObjectUtils.isNull(saggieRequestContext)) logStr += ".";
        else logStr = logStr + " from " + saggieRequestContext.toString();
        HttpServletRequestInfo httpServletRequestInfo = new HttpServletRequestInfo(request);
        logStr = logStr + " . Request Data : " + JSONUtils.toJSONWithoutClassName(httpServletRequestInfo);
        log(rlogger,  level, logStr);
    }

    public static void logRequestIn(Logger rlogger, Level level, SaggieRequestContext saggieRequestContext, String message){
        String logStr = " Incoming request";
        if(SaggieObjectUtils.isNull(saggieRequestContext)) logStr += ".";
        else logStr = logStr + " from " + saggieRequestContext.toString();
        logStr = logStr + " . " + message ;
        log(rlogger,  level, logStr);
    }

    public static void logRequestOut(Logger rlogger, Level level, SaggieRequestContext saggieRequestContext, HttpServletRequest request){
        String logStr = " Out request";
        if(SaggieObjectUtils.isNull(saggieRequestContext)) logStr += ".";
        else logStr = logStr + " from " + saggieRequestContext.toString();
        HttpServletRequestInfo httpServletRequestInfo = new HttpServletRequestInfo(request);
        logStr = logStr + " . Request Data : " + JSONUtils.toJSONWithoutClassName(httpServletRequestInfo);
        log(rlogger,  level, logStr);
    }

    public static void logRequestOut(Logger rlogger, Level level, SaggieRequestContext saggieRequestContext, String message){
        String logStr = " Out request";
        if(SaggieObjectUtils.isNull(saggieRequestContext)) logStr += ".";
        else logStr = logStr + " from " + saggieRequestContext.toString();
        logStr = logStr + " . " + message ;
        log(rlogger,  level, logStr);
    }

    public static void log(Logger rlogger, Level level, SaggieRequestContext saggieRequestContext, String message){
        String logStr = "";
        if(null!=saggieRequestContext) logStr = saggieRequestContext.toString();
        log(rlogger,  level, logStr + message);
    }

    public static void log(Logger rlogger, Level level, SaggieRequestContext saggieRequestContext, String message, Exception exception){
        String logStr = "";
        if(null!=saggieRequestContext) logStr = saggieRequestContext.toString();
        log(rlogger,  level, logStr + message, exception);
    }

    public static void log(Logger rlogger, Level level, String message){
        rlogger.log(level, message);
    }

    public static void log(Logger rlogger, Level level, String message, Exception exception){
        rlogger.log(level, message, exception);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    private static class HttpServletRequestInfo {
        private String contextPath;
        private String localAddr;
        private String localName;
        private Integer localPort;
        private String pathInfo;
        private String pathTranslated;
        private String protocol;
        private String remoteAddr;
        private String remoteHost;
        private Integer remotePort;
        private String requestURI;
        private String requestURL;
        private String scheme;
        private String serverName;
        private Integer serverPort;
        private String servletPath;

        public HttpServletRequestInfo(HttpServletRequest request) {
            this.contextPath = request.getContextPath();
            this.localAddr = request.getLocalAddr();
            this.localName = request.getLocalName();
            this.localPort = request.getLocalPort();
            this.pathInfo = request.getPathInfo();
            this.pathTranslated = request.getPathTranslated();
            this.protocol = request.getProtocol();
            this.remoteAddr = request.getRemoteAddr();
            this.remoteHost = request.getRemoteHost();
            this.remotePort = request.getRemotePort();
            this.requestURI = request.getRequestURI();
            this.requestURL = request.getRequestURL().toString();
            this.scheme = request.getScheme();
            this.serverName = request.getServerName();
            this.serverPort = request.getServerPort();
            this.servletPath = request.getServletPath();

        }
    }
}
