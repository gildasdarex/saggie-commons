package com.saggie.data.access.repositories;

import com.saggie.data.access.entities.AgentStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgentStatusRepository extends JpaRepository<AgentStatusEntity, Integer> {
}
