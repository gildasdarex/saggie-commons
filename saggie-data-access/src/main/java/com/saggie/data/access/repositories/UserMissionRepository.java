package com.saggie.data.access.repositories;

import com.saggie.data.access.entities.UserMissionEntity;
import com.saggie.data.access.entities.UserMissionEntityPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMissionRepository extends JpaRepository<UserMissionEntity, UserMissionEntityPK> {

    UserMissionEntity findOneByMissionId(Integer missionId);
    List<UserMissionEntity> findByUserByUserId_Username(String username);
    List<UserMissionEntity> findByUserId(Integer userId);

}
