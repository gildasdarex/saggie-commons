package com.saggie.data.access.repositories;

import com.saggie.data.access.entities.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Integer> {
    RoleEntity findByDescription(String description);
    RoleEntity findById(Integer id);
}
