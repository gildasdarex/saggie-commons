package com.saggie.data.access.repositories;

import com.saggie.data.access.entities.MissionStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MissionStatusRepository extends JpaRepository<MissionStatusEntity, Integer> {
}
