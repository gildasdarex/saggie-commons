package com.saggie.data.access.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class UserMissionEntityPK implements Serializable {
    private int userId;
    private int missionId;

    @Column(name = "user_id")
    @Id
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Column(name = "mission_id")
    @Id
    public int getMissionId() {
        return missionId;
    }

    public void setMissionId(int missionId) {
        this.missionId = missionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserMissionEntityPK that = (UserMissionEntityPK) o;
        return userId == that.userId &&
                missionId == that.missionId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, missionId);
    }
}
