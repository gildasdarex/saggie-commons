
package com.saggie.data.access.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_mission")
@IdClass(UserMissionEntityPK.class)
public class UserMissionEntity {
    private int userId;
    private int missionId;
    private int statusId;
    private UserEntity userByUserId;
    private MissionEntity missionByMissionId;
    private MissionStatusEntity missionStatusByStatusId;

    @Id
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Id
    @Column(name = "mission_id")
    public int getMissionId() {
        return missionId;
    }

    public void setMissionId(int missionId) {
        this.missionId = missionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserMissionEntity that = (UserMissionEntity) o;
        return userId == that.userId &&
                missionId == that.missionId &&
                Objects.equals(statusId, that.statusId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, missionId, statusId);
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false, updatable = false, insertable = false)
    public UserEntity getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(UserEntity userByUserId) {
        this.userByUserId = userByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "mission_id", referencedColumnName = "id", nullable = false, updatable = false, insertable = false)
    public MissionEntity getMissionByMissionId() {
        return missionByMissionId;
    }

    public void setMissionByMissionId(MissionEntity missionByMissionId) {
        this.missionByMissionId = missionByMissionId;
    }

    @Basic
    @Column(name = "status")
    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    @ManyToOne
    @JoinColumn(name = "status", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public MissionStatusEntity getMissionStatusByStatusId() {
        return missionStatusByStatusId;
    }

    public void setMissionStatusByStatusId(MissionStatusEntity missionStatusByStatusId) {
        this.missionStatusByStatusId = missionStatusByStatusId;
    }
}
