package com.saggie.data.access.repositories;

import com.saggie.data.access.entities.MissionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MissionRepository extends JpaRepository<MissionEntity, Integer> {
    List<MissionEntity> findByCreatedBy(Integer id);
    MissionEntity findById(Integer id);
}
