package com.saggie.data.access.repositories;

import com.saggie.data.access.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {
    UserEntity findByEmail(String email);
    UserEntity findById(Integer id);
    UserEntity findByUsername(String username);
    UserEntity findByUsernameAndStatus(String username, Integer id);
    List<UserEntity> findByRoleId(Integer roleId);
    List<UserEntity> findByRoleIdIn(List<Integer> roleIds);
}
