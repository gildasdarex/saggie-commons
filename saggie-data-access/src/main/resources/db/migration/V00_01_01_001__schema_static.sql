-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `saagie_imf`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `saagie_imf`.`role` ;

CREATE TABLE IF NOT EXISTS `saagie_imf`.`role` (
                                                 `id` INT NOT NULL,
                                                 `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `saagie_imf`.`agent_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `saagie_imf`.`agent_status` ;

CREATE TABLE IF NOT EXISTS `saagie_imf`.`agent_status` (
                                                         `id` INT NOT NULL,
                                                         `name` VARCHAR(45) NULL,
  `description` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `saagie_imf`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `saagie_imf`.`user` ;

CREATE TABLE IF NOT EXISTS `saagie_imf`.`user` (
                                                 `id` INT NOT NULL AUTO_INCREMENT,
                                                 `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `role_id` INT NOT NULL,
  `date_of_birth` TIMESTAMP NOT NULL,
  `status` INT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  `username` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `role_id_role_fk`
  FOREIGN KEY (`role_id`)
  REFERENCES `saagie_imf`.`role` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT `status_agent_status_fk`
  FOREIGN KEY (`status`)
  REFERENCES `saagie_imf`.`agent_status` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION)
  ENGINE = InnoDB;

CREATE INDEX `role_id_role_fk_idx` ON `saagie_imf`.`user` (`role_id` ASC);

CREATE UNIQUE INDEX `email_UNIQUE` ON `saagie_imf`.`user` (`email` ASC);

CREATE INDEX `status_agent_status_fk_idx` ON `saagie_imf`.`user` (`status` ASC);


-- -----------------------------------------------------
-- Table `saagie_imf`.`mission_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `saagie_imf`.`mission_status` ;

CREATE TABLE IF NOT EXISTS `saagie_imf`.`mission_status` (
                                                           `id` INT NOT NULL,
                                                           `name` VARCHAR(45) NULL,
  `description` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `saagie_imf`.`mission`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `saagie_imf`.`mission` ;

CREATE TABLE IF NOT EXISTS `saagie_imf`.`mission` (
                                                    `id` INT NOT NULL AUTO_INCREMENT,
                                                    `name` VARCHAR(45) NOT NULL,
  `start_date` TIMESTAMP NULL,
  `end_date` TIMESTAMP NULL,
  `status` INT NULL,
  `created_by` INT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `status_mission_status_fk`
  FOREIGN KEY (`status`)
  REFERENCES `saagie_imf`.`mission_status` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION)
  ENGINE = InnoDB;

CREATE INDEX `status_mission_status_fk_idx` ON `saagie_imf`.`mission` (`status` ASC);


-- -----------------------------------------------------
-- Table `saagie_imf`.`user_mission`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `saagie_imf`.`user_mission` ;

CREATE TABLE IF NOT EXISTS `saagie_imf`.`user_mission` (
                                                         `user_id` INT NOT NULL,
                                                         `mission_id` INT NOT NULL,
                                                         `user_status` VARCHAR(45) NULL,
                                                         `status` INT NOT NULL,


  PRIMARY KEY (`user_id`, `mission_id`),
  CONSTRAINT `user_id_user_fk`
  FOREIGN KEY (`user_id`)
  REFERENCES `saagie_imf`.`user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT `mission_id_mission_fk`
  FOREIGN KEY (`mission_id`)
  REFERENCES `saagie_imf`.`mission` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT `status_mission_fk`
  FOREIGN KEY (`status`)
  REFERENCES `saagie_imf`.`mission_status` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION)
  ENGINE = InnoDB;

CREATE INDEX `mission_id_mission_fk_idx` ON `saagie_imf`.`user_mission` (`mission_id` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `saagie_imf`.`role`
-- -----------------------------------------------------
START TRANSACTION;
USE `saagie_imf`;
INSERT INTO `saagie_imf`.`role` (`id`, `name`, `description`) VALUES (1, 'ROLE_ADMIN', 'admin');
INSERT INTO `saagie_imf`.`role` (`id`, `name`, `description`) VALUES (2, 'ROLE_LEADER', 'leader');
INSERT INTO `saagie_imf`.`role` (`id`, `name`, `description`) VALUES (3, 'ROLE_AGENT', 'agent');

COMMIT;


-- -----------------------------------------------------
-- Data for table `saagie_imf`.`agent_status`
-- -----------------------------------------------------
START TRANSACTION;
USE `saagie_imf`;
INSERT INTO `saagie_imf`.`agent_status` (`id`, `name`, `description`) VALUES (1, 'ON_MISSION', 'On Mission');
INSERT INTO `saagie_imf`.`agent_status` (`id`, `name`, `description`) VALUES (2, 'HOME', 'In Office');
INSERT INTO `saagie_imf`.`agent_status` (`id`, `name`, `description`) VALUES (3, 'NONE', 'None');
INSERT INTO `saagie_imf`.`agent_status` (`id`, `name`, `description`) VALUES (4, 'ALIVE', 'Alive');
INSERT INTO `saagie_imf`.`agent_status` (`id`, `name`, `description`) VALUES (5, 'DEAD', 'Dead');
INSERT INTO `saagie_imf`.`agent_status` (`id`, `name`, `description`) VALUES (6, 'MISSING', 'Missing');

COMMIT;


-- -----------------------------------------------------
-- Data for table `saagie_imf`.`mission_status`
-- -----------------------------------------------------
START TRANSACTION;
USE `saagie_imf`;
INSERT INTO `saagie_imf`.`mission_status` (`id`, `name`, `description`) VALUES (1, 'COMPLETED', 'Completed');
INSERT INTO `saagie_imf`.`mission_status` (`id`, `name`, `description`) VALUES (2, 'FAILED', 'failed');
INSERT INTO `saagie_imf`.`mission_status` (`id`, `name`, `description`) VALUES (3, 'IN_PROGRESS', 'In progress');
INSERT INTO `saagie_imf`.`mission_status` (`id`, `name`, `description`) VALUES (4, 'AGENT_NOT_FOUND', 'Agent missing');
INSERT INTO `saagie_imf`.`mission_status` (`id`, `name`, `description`) VALUES (5, 'AGENT_DEAD', 'Agent is dead');
INSERT INTO `saagie_imf`.`mission_status` (`id`, `name`, `description`) VALUES (6, 'NEW', 'New mission created');

COMMIT;

START TRANSACTION;
USE `saagie_imf`;
INSERT INTO `saagie_imf`.`user` (`id`, `first_name`, `last_name`, `email`, `status`, `username`, `password`, `role_id`, `date_of_birth`) VALUES ('1', 'James', 'Bond', 'jbond@imf.com', '4', 'jbond@imf.com', 'Test123$', '3', '1990-08-12 00:00:00');
INSERT INTO `saagie_imf`.`user` (`id`, `first_name`, `last_name`, `email`, `username`,`password`, `role_id`, `date_of_birth`) VALUES ('2', 'John', 'Decosta', 'johnd@imf.com', 'johnd@imf.com', 'Test123$', '2', '1985-02-14 00:00:00');
INSERT INTO `saagie_imf`.`user` (`id`, `first_name`, `last_name`, `email`, `username`,`password`, `role_id`, `date_of_birth`) VALUES ('3', 'David', 'Smith', 'davids@imf.com', 'davids@imf.com', 'Test123$', '1', '1977-09-03 00:00:00');

COMMIT ;