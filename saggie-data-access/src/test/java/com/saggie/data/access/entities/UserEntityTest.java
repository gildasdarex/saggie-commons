/**
 * Copyright (c) 2019, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by bilalshah on 2019-09-01
 */
package com.saggie.data.access.entities;

import com.saggie.data.access.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.annotation.Transactional;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.Month;

@ContextConfiguration(locations = {"/spring-config.xml"})
@Transactional
public class UserEntityTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void createUserTest() {
        UserEntity userEntity = new UserEntity();
        userEntity.setEmail("james.bond@imf.com");
        userEntity.setFirstName("James");
        userEntity.setLastName("Bond");
        userEntity.setDateOfBirth(Timestamp.valueOf(LocalDate.of(1990, Month.APRIL, 18 ).atTime(0, 0)));
        userEntity.setPassword("Test123$");
        userEntity.setRoleId(1);

        userEntity = userRepository.save(userEntity);

        Assert.assertNotEquals(userEntity.getId(), 0);
        Assert.assertEquals(userEntity.getEmail(), "james.bond@imf.com");

        userRepository.delete(userEntity);
    }
}
